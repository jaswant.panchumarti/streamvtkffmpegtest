#pragma once

#define WIDTH 1920
#define HEIGHT 1080

/* 1: low quality 10: high quality */
#define QUALITY 10

#define BIT_RATE 1600000 * QUALITY // max: 16Mbps, min: 1.6 Mbps

/* The encoder will enforce an I-Frame if the time since previous I-Frame is larger than this
 * number */
#define IFRAME_MAXIMUM_INTERVAL_MSEC 2000

// 1: vp9
// 2: AV1
// 3: H264
// 4: HEVC/H.265
#define CODEC 1

/* The encoder accepts a maximum number of frames after which it returns EOF */
#define MAXIMUM_FRAMES_ENCODED 30

/* The encoder will do it's thing over group of pictures for this size */
#define GOP_SIZE MAXIMUM_FRAMES_ENCODED / (QUALITY + 5)

/* Use vtkJPEGWriter/vtkJPEGReader instead of the FFMPEG encoder/decoder. */
#define USE_VTK_JPEG 0
/* Use vtkPNGWriter/vtkPNGReader instead of the FFMPEG encoder/decoder. */
#define USE_VTK_PNG 0

#if CODEC == 1
#define ENCODERCODECTYPE VTKCodecType::VP9
#define DECODERCODECTYPE VTKCodecType::VP9
#elif CODEC == 2
#define ENCODERCODECTYPE VTKCodecType::AV1
#define DECODERCODECTYPE VTKCodecType::AV1
#elif CODEC == 3
#define ENCODERCODECTYPE VTKCodecType::H264
#define DECODERCODECTYPE VTKCodecType::H264
#elif CODEC == 4
#define ENCODERCODECTYPE VTKCodecType::H265
#define DECODERCODECTYPE VTKCodecType::H265
#endif