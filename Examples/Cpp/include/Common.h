#pragma once
#include <cstdint>
#include <string>

#include <vtkBMPWriter.h>
#include <vtkCamera.h>
#include <vtkImageData.h>
#include <vtkLogger.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkUnsignedCharArray.h>

#include <vtk_fmt.h>
// clang-format off
#include VTK_FMT(fmt/core.h)
// clang-format on

static const char* startMsg = "start";
static const char* finishMsg = "finish";

namespace vtk_helpers
{

inline void SaveState(double state[18], vtkRenderer* renderer)
{
  vtkCamera* cam = renderer->GetActiveCamera();
  cam->GetPosition(state);                               // 3
  cam->GetFocalPoint(&state[3]);                         // 3
  cam->GetViewUp(&state[6]);                             // 3
  cam->GetWindowCenter(&state[9]);                       // 2
  cam->GetClippingRange(&state[11]);                     // 2
  state[13] = cam->GetViewAngle();                       // 1
  state[14] = cam->GetParallelScale();                   // 1
  state[15] = cam->GetParallelProjection();              // 1
  state[16] = renderer->GetRenderWindow()->GetSize()[0]; // 1
  state[17] = renderer->GetRenderWindow()->GetSize()[1]; // 1
}

inline void LoadState(double state[18], vtkRenderer* renderer)
{
  vtkCamera* cam = renderer->GetActiveCamera();
  cam->SetPosition(state);                   // 3
  cam->SetFocalPoint(&state[3]);             // 3
  cam->SetViewUp(&state[6]);                 // 3
  cam->SetWindowCenter(state[9], state[10]); // 2
  // cam->SetClippingRange(&state[11]);            // 2
  // cam->SetViewAngle(state[13]);                 // 1
  // cam->SetParallelScale(state[14]);             // 1
  cam->SetParallelProjection(state[15] == 1.0);               // 1
  renderer->GetRenderWindow()->SetSize(state[16], state[17]); // 2
}

inline void PrintState(double state[18])
{
  cout << "Position : " << state[0] << ' ' << state[1] << ' ' << state[2] << '\n';
  cout << "FocalPoint : " << state[3] << ' ' << state[4] << ' ' << state[5] << '\n';
  cout << "ViewUp : " << state[6] << ' ' << state[7] << ' ' << state[8] << '\n';
  cout << "WindowCenter : " << state[9] << ' ' << state[10] << '\n';
  cout << "ClippingRange : " << state[11] << ' ' << state[12] << '\n';
  cout << "ViewAngle : " << state[13] << '\n';
  cout << "ParallelScale : " << state[14] << '\n';
  cout << "ParallelProjection : " << state[15] << '\n';
  cout << "Viewport dimensions : " << state[16] << 'x' << state[17] << '\n';
}

inline void SaveImage(int w, int h, vtkUnsignedCharArray* data, const char* fname)
{
  vtkNew<vtkImageData> img;
  img->SetDimensions(w, h, 1);
  img->AllocateScalars(data->GetDataType(), data->GetNumberOfComponents());
  memcpy(
    img->GetScalarPointer(), data->GetPointer(0), data->GetDataSize() * data->GetDataTypeSize());
  vtkNew<vtkBMPWriter> writer;
  writer->SetInputData(img);
  writer->SetFileName(fname);
  writer->Write();
}

}

namespace nice_fmt
{
inline std::string getNiceTime(vtkIdType time_ns)
{
  if (time_ns > 1e9)
  {
    return fmt::format("{:>3}s", vtkIdType(time_ns / 1e9));
  }
  else if (time_ns > 1e6)
  {
    return fmt::format("{:>3}ms", vtkIdType(time_ns / 1e6));
  }
  else if (time_ns > 1e3)
  {
    return fmt::format("{:>3}us", vtkIdType(time_ns / 1e3));
  }
  else
  {
    return fmt::format("{:>3}ns", time_ns);
  }
}

inline std::string getNiceBytes(int num_bytes)
{
  if (num_bytes > 1024 * 1024 * 1024)
  {
    return fmt::format("{:>3}GB", int(num_bytes / (1024 * 1024 * 1024)));
  }
  else if (num_bytes > 1024 * 1024)
  {
    return fmt::format("{:>3}MB", int(num_bytes / (1024 * 1024)));
  }
  else if (num_bytes > 1024)
  {
    return fmt::format("{:>3}KB", int(num_bytes / 1024));
  }
  else
  {
    return fmt::format("{:>3}By", num_bytes);
  }
}
}
