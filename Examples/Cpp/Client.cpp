#include "vtkCodedVideoPacket.h"
#include "vtkRawVideoFrame.h"
#include <chrono>
#include <cstdlib>
#include <string>
#include <vtkImageData.h>
#include <vtkJPEGReader.h>

#if defined(_WIN32) && !defined(__CYGWIN__)
#define VTK_WINDOWS_FULL
#include "vtkWindows.h"
#define WSA_VERSION MAKEWORD(1, 1)
#endif

#include <vtkActor.h>
#include <vtkCallbackCommand.h>
#include <vtkCamera.h>
#include <vtkClientSocket.h>
#include <vtkCommand.h>
#include <vtkCylinderSource.h>
#include <vtkFFMPEGSoftwareDecoder.h>
#include <vtkInteractorStyle.h>
#include <vtkLogger.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkObject.h>
#include <vtkOpenGLRenderWindow.h>
#include <vtkOpenGLState.h>
#include <vtkPNGReader.h>
#include <vtkPointData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderTimerLog.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>
#include <vtkTextRepresentation.h>
#include <vtkUnsignedCharArray.h>
#include <vtk_glew.h>

#include "Common.h"
#include "Config.h"

namespace
{

// VTK objects.
vtkNew<vtkClientSocket> client;
vtkNew<vtkFFMPEGSoftwareDecoder> decoder;
vtkNew<vtkRenderer> renderer;
vtkNew<vtkRenderer> renderer2D;
vtkNew<vtkTextRepresentation> cornerAnnotation;
vtkNew<vtkRenderWindow> renWin;
vtkNew<vtkRenderWindowInteractor> rwi;
vtkNew<vtkCodedVideoPacket> Packet;
#if USE_VTK_JPEG
vtkNew<vtkJPEGReader> jpegReader;
vtkImageData* decompressedJpegImage = nullptr;
#elif USE_VTK_PNG
vtkNew<vtkPNGReader> pngReader;
vtkImageData* decompressedPngImage = nullptr;
#endif
vtkMTimeType LastCameraMTime = 0;
int LastSize[2] = { -1, -1 };
vtkSmartPointer<vtkRawVideoFrame> rgba32Frame = nullptr;

// utils
int RWObserverId;
std::vector<int> RWIObserverIds;
// time taken to copy a frame from the cpu to GPU with OpenGL.
std::chrono::high_resolution_clock::time_point t1Latency, t2Latency, t1PushToView, t2PushToView;

// Averages. Printed when client exits.
vtkIdType PushToViewAvgT = 0, DecodeAvgT = 0, ScaleAvgT = 0, RecvAvgT = 0, LatencyAvgT = 0;
int64_t totalNumFrames = 0;

void PushImagetoViewport(vtkObject*, unsigned long, void*, void*)
{
  vtkLogScopeFunction(TRACE);
  t1PushToView = std::chrono::high_resolution_clock::now();

#if USE_VTK_JPEG
  if (decompressedJpegImage == nullptr)
#elif USE_VTK_PNG
  if (decompressedPngImage == nullptr)
#else
  if (rgba32Frame == nullptr)
#endif
  {
    t2PushToView = std::chrono::high_resolution_clock::now();
    vtkLog(ERROR, "Frame is null");
    return;
  }
  int size[2], low_point[2];
  renderer->GetTiledSizeAndOrigin(&size[0], &size[1], &low_point[0], &low_point[1]);
  if (size[0] <= 0 || size[1] <= 0)
  {
    t2PushToView = std::chrono::high_resolution_clock::now();
    vtkGenericWarningMacro("Viewport empty. Cannot push to screen.");
    rgba32Frame = nullptr;
    return;
  }

  vtkOpenGLState* ostate =
    static_cast<vtkOpenGLRenderWindow*>(renderer->GetVTKWindow())->GetState();
  ostate->vtkglEnable(GL_SCISSOR_TEST);
  ostate->vtkglViewport(low_point[0], low_point[1], size[0], size[1]);
  ostate->vtkglScissor(low_point[0], low_point[1], size[0], size[1]);

  // framebuffers have their color premultiplied by alpha.
  vtkOpenGLState::ScopedglBlendFuncSeparate bfsaver(ostate);
  vtkOpenGLState::ScopedglEnableDisable bsaver(ostate, GL_BLEND);
  if (true)
  {
    vtkLogF(TRACE, "PushToFrameBuffer: using blend");
    ostate->vtkglEnable(GL_BLEND);
    ostate->vtkglBlendFuncSeparate(GL_ONE, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
  }
  else
  {
    ostate->vtkglDisable(GL_BLEND);
    vtkLogF(TRACE, "PushToFrameBuffer: not-using blend");
  }
  vtkOpenGLRenderWindow* oglRenWin = vtkOpenGLRenderWindow::SafeDownCast(renWin);
#if USE_VTK_JPEG
  oglRenWin->DrawPixels(low_point[0], low_point[1], low_point[0] + size[0] - 1,
    low_point[1] + size[1] - 1, 0, 0, decompressedJpegImage->GetDimensions()[0] - 1,
    decompressedJpegImage->GetDimensions()[1] - 1, decompressedJpegImage->GetDimensions()[0],
    decompressedJpegImage->GetDimensions()[1], 4, VTK_UNSIGNED_CHAR,
    decompressedJpegImage->GetScalarPointer());
#elif USE_VTK_PNG
  oglRenWin->DrawPixels(low_point[0], low_point[1], low_point[0] + size[0] - 1,
    low_point[1] + size[1] - 1, 0, 0, decompressedPngImage->GetDimensions()[0] - 1,
    decompressedPngImage->GetDimensions()[1] - 1, decompressedPngImage->GetDimensions()[0],
    decompressedPngImage->GetDimensions()[1], 4, VTK_UNSIGNED_CHAR,
    decompressedPngImage->GetScalarPointer());
#else
  if (rgba32Frame->GetWidth() == 0 || rgba32Frame->GetHeight() == 0)
  {
    t2PushToView = std::chrono::high_resolution_clock::now();
    vtkLog(ERROR, "Size messed up");
    rgba32Frame = nullptr;
    return;
  }
  vtkLogF(TRACE, "Args %d, %d, %d, %d, %d, %d, %d, %d, %d, %d", low_point[0], low_point[1],
    low_point[0] + size[0] - 1, low_point[1] + size[1] - 1, 0, 0, rgba32Frame->GetWidth() - 1,
    rgba32Frame->GetHeight() - 1, rgba32Frame->GetWidth(), rgba32Frame->GetHeight());
  renderer->Clear();
  // unsigned char* buffer = nullptr;
  // rgba32Frame->GetData(buffer);
  // oglRenWin->DrawPixels(low_point[0], low_point[1], low_point[0] + size[0] - 1,
  //   low_point[1] + size[1] - 1, 0, 0, rgba32Frame->GetWidth() - 1, rgba32Frame->GetHeight() - 1,
  //   rgba32Frame->GetWidth(), rgba32Frame->GetHeight(), 4, VTK_UNSIGNED_CHAR, buffer);
  unsigned char* buffers[3];
  rgba32Frame->GetData(buffers[0], 0);
  rgba32Frame->GetData(buffers[1], 1);
  rgba32Frame->GetData(buffers[2], 2);
  oglRenWin->DrawYCbCr420Pixels(
    rgba32Frame->GetWidth(), rgba32Frame->GetHeight(), buffers, rgba32Frame->GetStrides());

#endif
  t2PushToView = std::chrono::high_resolution_clock::now();
}

void OnNewDecodedFrame(vtkObject*, unsigned long, void*, void* callData)
{
  vtkLogScopeFunction(TRACE);
  rgba32Frame = reinterpret_cast<vtkRawVideoFrame*>(callData);
  if (!rgba32Frame)
  {
    vtkLog(ERROR, << "Got unexpected frame type");
    return;
  }
}

bool UpdateStateIfNeeded()
{
  vtkLogScopeFunction(TRACE);
  if (LastCameraMTime >= renderer->GetActiveCamera()->GetMTime() &&
    (LastSize[0] == renWin->GetSize()[0] && LastSize[1] == renWin->GetSize()[1]))
  {
    return false;
  }
  LastCameraMTime = renderer->GetActiveCamera()->GetMTime();
  LastSize[0] = renWin->GetSize()[0];
  LastSize[1] = renWin->GetSize()[1];

  // push our camera to the server.
  double state[18];
  vtk_helpers::SaveState(state, renderer);
  client->Send(state, sizeof(state));
  vtkLog(TRACE, << "Sent state");

  return true;
}

void ReceiveEncodedPackets()
{
  bool start = false;
  bool finished = false;
  /* Look for magic start message */
  char startBuf[5] = {};
  char finishBuf[6] = {};
  std::chrono::high_resolution_clock::duration dtReceive;
  do
  {
    int pktSize = 0;
    int recvMsgSize = client->Receive(&pktSize, sizeof(pktSize));
    {
      vtkLog(TRACE, << "recvMsgSize " << recvMsgSize);
      vtkLog(TRACE, << "pktSize " << pktSize);
    }
    switch (pktSize)
    {
      case 5:
        client->Receive(startBuf, pktSize);
        if (std::string(startBuf, pktSize) == startMsg)
        {
          vtkLog(TRACE, << "Received start message");
          start = true;
          continue;
        }
        break;
      case 6:
        client->Receive(finishBuf, pktSize);
        if (std::string(finishBuf, pktSize) == finishMsg)
        {
          vtkLog(TRACE, << "Received finish message");
          finished = true;
          break;
        }
        break;
      default:
      {
        ::Packet->SetSize(pktSize);
        unsigned char* recvBuffer = nullptr;
        ::Packet->GetData(recvBuffer);

        auto t = std::chrono::high_resolution_clock::now();
        recvMsgSize = client->Receive(recvBuffer, pktSize);
        dtReceive = std::chrono::high_resolution_clock::now() - t;

        if (recvMsgSize == 0)
        {
          finished = true;
        }
        else
        {
          ++totalNumFrames;
        }
      }
      break;
    }
    if (start && !finished)
    {
#if USE_VTK_JPEG
      auto t1 = std::chrono::high_resolution_clock::now();
      jpegReader->SetMemoryBufferLength(::Packet->GetNumberOfValues());
      jpegReader->SetMemoryBuffer(::Packet->GetPointer(0));
      jpegReader->SetDataScalarTypeToUnsignedChar();
      jpegReader->SetDataByteOrderToLittleEndian();
      jpegReader->Update();
      auto jpegDecodeTime = std::chrono::high_resolution_clock::now() - t1;
      decompressedJpegImage = jpegReader->GetOutput();
#elif USE_VTK_PNG
      auto t1 = std::chrono::high_resolution_clock::now();
      pngReader->SetMemoryBufferLength(::Packet->GetNumberOfValues());
      pngReader->SetMemoryBuffer(::Packet->GetPointer(0));
      pngReader->SetDataScalarTypeToUnsignedChar();
      pngReader->SetDataByteOrderToLittleEndian();
      pngReader->Update();
      auto pngDecodeTime = std::chrono::high_resolution_clock::now() - t1;
      decompressedPngImage = pngReader->GetOutput();
#else
      if (!decoder->Push(::Packet))
      {
        vtkLog(ERROR, << "Failed to decode packet.");
      }
      auto result = decoder->GetResult();
      if (result != nullptr)
      {
        ::rgba32Frame = result;
      }
#endif
      vtkLog(TRACE, "T2");
      t2Latency = std::chrono::high_resolution_clock::now();
      std::string stats;
      stats += "Stats (Client)\n";
      stats += "Latency " + nice_fmt::getNiceTime((t2Latency - t1Latency).count()) + '\n';
      stats +=
        "CPU->GPU copy  " + nice_fmt::getNiceTime((t2PushToView - t1PushToView).count()) + '\n';
#if USE_VTK_JPEG
      stats += "Decode  " + nice_fmt::getNiceTime(jpegDecodeTime.count()) + '\n';
#elif USE_VTK_PNG
      stats += "Decode  " + nice_fmt::getNiceTime(pngDecodeTime.count()) + '\n';
#else
      stats += "Decode  " + nice_fmt::getNiceTime(decoder->GetLastDecodeTimeNS()) + '\n';
      stats += "SwScale     " + nice_fmt::getNiceTime(decoder->GetLastScaleTimeNS()) + '\n';
#endif
      stats += "Recvd " + nice_fmt::getNiceBytes(::Packet->GetSize()) + " in " +
        nice_fmt::getNiceTime(dtReceive.count()) + '\n';
      PushToViewAvgT += (t2PushToView - t1PushToView).count();
#if USE_VTK_JPEG
      DecodeAvgT += jpegDecodeTime.count();
#elif USE_VTK_PNG
      DecodeAvgT += pngDecodeTime.count();
#else
      DecodeAvgT += decoder->GetLastDecodeTimeNS();
      ScaleAvgT += decoder->GetLastScaleTimeNS();
#endif
      RecvAvgT += dtReceive.count();
      LatencyAvgT += (t2Latency - t1Latency).count();
      cornerAnnotation->SetText(stats.c_str());
    }
  } while (!finished);
}

void HandleInteractorEvent(vtkObject*, unsigned long ev, void*, void*)
{
  vtkLogScopeFunction(TRACE);
  vtkLog(TRACE, "T1");
  t1Latency = std::chrono::high_resolution_clock::now();
  bool sent = false;
  switch (ev)
  {
    case vtkCommand::RenderEvent:
    case vtkCommand::WindowResizeEvent:
    case vtkCommand::ConfigureEvent:
      sent = UpdateStateIfNeeded();
      break;
  }

  if (sent)
  {
    ::ReceiveEncodedPackets();
  }
}

void SetupInteractors(vtkObject*, unsigned long, void*, void*)
{
  vtkLogScopeFunction(TRACE);
  for (const auto& ev :
    { vtkCommand::RenderEvent, vtkCommand::WindowResizeEvent, vtkCommand::ConfigureEvent })
  {
    vtkNew<vtkCallbackCommand> cmd;
    cmd->SetCallback(&HandleInteractorEvent);
    RWIObserverIds.push_back(rwi->AddObserver(ev, cmd));
  }
  // Remove the observer so we don't end up calling this method again.
  renWin->RemoveObserver(RWObserverId);
  RWObserverId = 0;
}

} // namespace

int main(int argc, char* argv[])
{
  vtkLogger::SetThreadName("Client");
  vtkLogger::SetStderrVerbosity(vtkLogger::VERBOSITY_9);

  if (argc < 3)
  { // Test for correct number of arguments
    vtkLog(ERROR, << "Usage: " << argv[0] << " <Server> <Server Port>\n");
    exit(1);
  }

  std::string serverAddress = argv[1];
  int serverPort = std::stoi(argv[2]);

#if defined(_WIN32) && !defined(__CYGWIN__)
  WSAData wsaData;
  if (WSAStartup(WSA_VERSION, &wsaData))
  {
    vtkLog(ERROR, "Could not initialize sockets !");
  }
#endif

  renWin->SetSize(WIDTH, HEIGHT);
  renWin->AddRenderer(renderer);
  renWin->SetWindowName("Client");
  rwi->SetRenderWindow(renWin);
  rwi->Initialize();

  // corner annotation.
  renWin->AddRenderer(renderer2D);
  cornerAnnotation->SetText("Stats (Client)");
  cornerAnnotation->SetRenderer(renderer2D);
  cornerAnnotation->BuildRepresentation();
  cornerAnnotation->SetWindowLocation(vtkTextRepresentation::UpperRightCorner);
  cornerAnnotation->GetTextActor()->SetTextScaleModeToNone();
  cornerAnnotation->GetTextActor()->GetTextProperty()->SetBackgroundRGBA(0.1, 0.1, 0.1, 0.2);
  cornerAnnotation->SetShowBorder(true);
  cornerAnnotation->SetBorderColor(0.84313725, 0.75686275, 0.90980392);
  cornerAnnotation->SetCornerRadiusStrength(0.1);
  cornerAnnotation->SetPadding(4);
  cornerAnnotation->GetTextActor()->GetTextProperty()->SetFontFamilyToCourier();
  cornerAnnotation->GetTextActor()->GetTextProperty()->SetFontSize(24);
  cornerAnnotation->GetTextActor()->GetTextProperty()->SetJustificationToRight();
  cornerAnnotation->SetVisibility(true);
  renWin->SetNumberOfLayers(2);
  renderer2D->AddActor(cornerAnnotation);
  renderer2D->SetLayer(1);
  renderer2D->EraseOff();
  renderer2D->InteractiveOff();
  renWin->AddRenderer(renderer2D);

  if (client->ConnectToServer(serverAddress.c_str(), serverPort) == -1)
  {
    vtkLog(ERROR, << "Failed to connect " << serverAddress << ':' << serverPort);
    return EXIT_FAILURE;
  }
  else
  {
    vtkLog(INFO, << "Connected to " << serverAddress << ':' << serverPort);
    // get initial state from server.
    double state[18];
    client->Receive(state, sizeof(state));
    vtkLog(TRACE, << "Got initial state");
    vtkLog(TRACE, << "--------------------------------");
    vtk_helpers::LoadState(state, renderer);
  }

  vtkNew<vtkCallbackCommand> onEmitFrameEvent;
  decoder->SetCodecType(DECODERCODECTYPE);
  // decoder->AddObserver(vtkCommand::ProgressEvent, onEmitFrameEvent);
  // onEmitFrameEvent->SetCallback(&OnNewDecodedFrame);

  vtkNew<vtkCallbackCommand> pushCmd;
  pushCmd->SetCallback(&PushImagetoViewport);
  renderer->AddObserver(vtkCommand::EndEvent, pushCmd);

  vtkNew<vtkCallbackCommand> onRenWinModifiedCmd;
  onRenWinModifiedCmd->SetCallback(&SetupInteractors);
  RWObserverId = renWin->AddObserver(vtkCommand::ModifiedEvent, onRenWinModifiedCmd);
  // Summary of what we just did.
  // decoder will call Render() on the render window for each new frame.
  // PushImageToViewPort is called when renderer invokes EndEvent (happens towards the end of
  // vtkRenderer::Render) Whenever, render window is modified, setup interactor event observers to
  // HandleInteractorEvent

  // force a push-pull. this makes sure that HandleInteractor is connected.
  // and our window displays an image from the server upon first show.
  ::renWin->InvokeEvent(vtkCommand::ModifiedEvent); // connects HandleInteractorEvent
  ::rwi->Render();
  ::rwi->Start();

  std::cout << "Stats summary:\n";
  std::cout << "Total number of frames: " << totalNumFrames << '\n';
  std::cout << "Average Latency: " << nice_fmt::getNiceTime(LatencyAvgT / totalNumFrames) << '\n';
  std::cout << "Average CPU->GPU copy time: "
            << nice_fmt::getNiceTime(PushToViewAvgT / totalNumFrames) << '\n';
  std::cout << "Average Decode time: " << nice_fmt::getNiceTime(DecodeAvgT / totalNumFrames)
            << '\n';
  std::cout << "Average Scale time: " << nice_fmt::getNiceTime(ScaleAvgT / totalNumFrames) << '\n';
  std::cout << "Average Recv time: " << nice_fmt::getNiceTime(RecvAvgT / totalNumFrames) << '\n';

  /* Cleanup observers */
  renderer->RemoveObserver(vtkCommand::EndEvent);
  for (auto oid : RWIObserverIds)
  {
    rwi->RemoveObserver(oid);
  }

  client->CloseSocket();
  return EXIT_SUCCESS;
}
