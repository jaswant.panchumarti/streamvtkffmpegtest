#include <array>
#include <chrono>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>

#if defined(_WIN32) && !defined(__CYGWIN__)
#define VTK_WINDOWS_FULL
#include "vtkWindows.h"
#define WSA_VERSION MAKEWORD(1, 1)
#endif

#include "vtkAbstractVideoEncoder.h"
#include "vtkCodedVideoPacket.h"
#include "vtkFFMPEGHardwareEncoder.h"
#include "vtkFFMPEGSoftwareEncoder.h"
#include "vtkRawVideoFrame.h"
#include "vtkWEBMWriter.h"

#include <vtkActor.h>
#include <vtkCallbackCommand.h>
#include <vtkCamera.h>
#include <vtkClientSocket.h>
#include <vtkCommand.h>
#include <vtkCylinderSource.h>
#include <vtkImageData.h>
#include <vtkInformation.h>
#include <vtkJPEGWriter.h>
#include <vtkLogger.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkPNGWriter.h>
#include <vtkPointData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkServerSocket.h>
#include <vtkSmartPointer.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>
#include <vtkTextRepresentation.h>
#include <vtkUnsignedCharArray.h>

#include "Common.h"
#include "Config.h"

namespace
{

// VTK objects.
vtkNew<vtkUnsignedCharArray> RGBA32Data;
vtkNew<vtkImageData> image;
vtkNew<vtkRawVideoFrame> videoFrame;
vtkNew<vtkRenderer> renderer;
vtkNew<vtkRenderer> renderer2D;
vtkNew<vtkTextRepresentation> cornerAnnotation;
vtkNew<vtkRenderWindow> renWin;
vtkNew<vtkRenderWindowInteractor> rwi;
vtkNew<vtkWEBMWriter> webmWriter;

#if USE_VTK_JPEG
vtkNew<vtkJPEGWriter> jpegWriter;
#elif USE_VTK_PNG
vtkNew<vtkPNGWriter> pngWriter;
#endif

// time taken to capture a frame on the gpu and copy it to a buffer on the cpu.
std::chrono::high_resolution_clock::time_point t1Capture, t2Capture;
#if USE_VTK_JPEG
std::chrono::high_resolution_clock::time_point t1JpegEnc, t2JpegEnc;
#elif USE_VTK_PNG
std::chrono::high_resolution_clock::time_point t1PngEnc, t2PngEnc;
#endif

// Averages. Printed when client disconnects. Reset upon disconnect.
vtkIdType CaptureAvgT = 0, EncodeAvgT = 0, ScaleAvgT = 0, SendAvgT = 0, CRAvg = 0;
int64_t timestamp = 1, totalNumFrames = 0;

// copied from
// VTK/Rendering/Parallel/vtkSynchronizedRenderers::vtkRawImage::Capture with
// one modification - removed this pointer acccess
void Capture(vtkRenderer* ren)
{
  t1Capture = std::chrono::high_resolution_clock::now();
  double viewport[4];
  ren->GetViewport(viewport);

  int window_size[2];
  window_size[0] = ren->GetVTKWindow()->GetActualSize()[0];
  window_size[1] = ren->GetVTKWindow()->GetActualSize()[1];

  int viewport_in_pixels[4];
  viewport_in_pixels[0] = static_cast<int>(window_size[0] * viewport[0]);
  viewport_in_pixels[1] = static_cast<int>(window_size[1] * viewport[1]);
  viewport_in_pixels[2] = static_cast<int>(window_size[0] * viewport[2]) - 1;
  viewport_in_pixels[3] = static_cast<int>(window_size[1] * viewport[3]) - 1;

  // we need to ensure that the size computation is always done in pixels,
  // otherwise we end up with rounding issues. In short, avoid doing
  // additions/subtractions using normalized viewport coordinates. Those are
  // better done in pixels.
  int image_size[2];
  image_size[0] = viewport_in_pixels[2] - viewport_in_pixels[0] + 1;
  image_size[1] = viewport_in_pixels[3] - viewport_in_pixels[1] + 1;

  // using RGBA always?
  // this->Resize(image_size[0], image_size[1], 4);
  ::RGBA32Data->SetNumberOfComponents(4);
  ::RGBA32Data->SetNumberOfTuples(image_size[0] * image_size[1]);

  ren->GetRenderWindow()->GetRGBACharPixelData(viewport_in_pixels[0], viewport_in_pixels[1],
    viewport_in_pixels[2], viewport_in_pixels[3], 0, ::RGBA32Data,
    /*right=*/ren->GetActiveCamera()->GetLeftEye() == 0);
  image->SetDimensions(image_size[0], image_size[1], 1);
  image->GetPointData()->SetScalars(::RGBA32Data);
  videoFrame->SetWidth(image_size[0]);
  videoFrame->SetHeight(image_size[1]);
  videoFrame->SetArray(::RGBA32Data);
  videoFrame->SetSliceOrder(vtkRawVideoFrame::SliceOrderType::BottomUp);
  videoFrame->SetPresentationTS(timestamp);
  t2Capture = std::chrono::high_resolution_clock::now();
}

int WriteOutputPacket(const char* prefix, bool isKey)
{
  vtkLogScopeFunction(TRACE);

  vtkUnsignedCharArray* dynBuf = webmWriter->GetDynamicBuffer();
  std::stringstream fname;
  fname << prefix << '-' << std::setfill('0') << std::setw(3) << totalNumFrames
        << /*(isKey ? "-i" : "-p") <<*/ ".bin";
  std::fstream file(
    fname.str(), std::fstream::binary | std::fstream::in | std::fstream::out | std::fstream::trunc);
  if (file.is_open() && dynBuf->GetNumberOfValues())
  {
    file.write(reinterpret_cast<char*>(dynBuf->GetPointer(0)), dynBuf->GetNumberOfValues());
    file.close();
  }
  int bytesWritten = dynBuf->GetNumberOfValues();
  webmWriter->Flush();
  return bytesWritten;
}

void TransmitEncodedPacket(
  vtkObject* object, unsigned long eventId, void* clientData, void* callData)
{
  vtkLogScopeFunction(TRACE);
#if !USE_VTK_JPEG && !USE_VTK_PNG
  auto encoder = vtkAbstractVideoEncoder::SafeDownCast(object);
  if (encoder == nullptr)
  {
    return;
  }
  if (eventId != vtkCommand::ProgressEvent)
  {
    return;
  }
#endif
  auto client = vtkSocket::SafeDownCast(reinterpret_cast<vtkObjectBase*>(clientData));
  if (!client)
  {
    return;
  }
  auto packet = reinterpret_cast<vtkCodedVideoPacket*>(callData);
  if (packet == nullptr)
  {
    return;
  }
  // if (packet->GetIsKeyFrame())
  // {
  //   if (packet->GetPresentationTS() > 1)
  //   {
  //     webmWriter->WriteFileTrailer();
  //   }
  // }
  // if (!webmWriter->GetIsHeaderWritten())
  // {
  //   webmWriter->SetWidth(packet->GetWidth());
  //   webmWriter->SetHeight(packet->GetHeight());
  //   webmWriter->SetFramerate(encoder->GetTimeBaseEnd());
  //   webmWriter->WriteVP9FileHeader();
  // }
  // webmWriter->WriteWebmBlock(packet);
  // if (webmWriter->GetWriteToMemory())
  // {
  //   ::WriteOutputPacket("frame", packet->GetIsKeyFrame());
  // }
  unsigned char* buffer = nullptr;
  const int length = packet->GetData(buffer);
  client->Send(&length, sizeof(length));

  auto t = std::chrono::high_resolution_clock::now();
  client->Send(buffer, length);
  auto dtSend = std::chrono::high_resolution_clock::now() - t;
  vtkLog(TRACE, << "Sent packet - " << length << "bytes");
  vtkLog(TRACE, << "================================");

  std::string stats;
  double cr = length > 0 ? ::RGBA32Data->GetNumberOfValues() / static_cast<double>(length)
                         : 0; // compression ratio
  stats += "Stats (Server)\n";
  stats += "Compression  " + std::to_string(int(cr)) + "x\n";
  stats += "GPU->CPU copy " + nice_fmt::getNiceTime((t2Capture - t1Capture).count()) + '\n';
#if USE_VTK_JPEG
  stats += "Encode      " + nice_fmt::getNiceTime((t2JpegEnc - t1JpegEnc).count()) + '\n';
#elif USE_VTK_PNG
  stats += "Encode      " + nice_fmt::getNiceTime((t2PngEnc - t1PngEnc).count()) + '\n';
#else
  stats += "Encode      " + nice_fmt::getNiceTime(encoder->GetLastEncodeTimeNS()) + '\n';
  stats += "SwScale     " + nice_fmt::getNiceTime(encoder->GetLastScaleTimeNS()) + '\n';
#endif
  stats += "Sent " + nice_fmt::getNiceBytes(length) + " in " +
    nice_fmt::getNiceTime(dtSend.count()) + '\n';
  CaptureAvgT += (t2Capture - t1Capture).count();
#if USE_VTK_JPEG
  EncodeAvgT += (t2JpegEnc - t1JpegEnc).count();
#elif USE_VTK_PNG
  EncodeAvgT += (t2PngEnc - t1PngEnc).count();
#else
  EncodeAvgT += encoder->GetLastEncodeTimeNS();
  ScaleAvgT += encoder->GetLastScaleTimeNS();
#endif
  SendAvgT += dtSend.count();
  CRAvg += cr;
  cornerAnnotation->SetText(stats.c_str());
}

// https://kitware.github.io/vtk-examples/site/Cxx/GeometricObjects/CylinderExample/
void SetupVTKPipeline()
{
  vtkNew<vtkNamedColors> colors;

  std::array<unsigned char, 4> bkg{ { 26, 51, 102, 255 } };
  colors->SetColor("BkgColor", bkg.data());

  vtkNew<vtkCylinderSource> cylinder;
  cylinder->SetResolution(8);

  vtkNew<vtkPolyDataMapper> cylinderMapper;
  cylinderMapper->SetInputConnection(cylinder->GetOutputPort());

  vtkNew<vtkActor> cylinderActor;
  cylinderActor->SetMapper(cylinderMapper);
  cylinderActor->GetProperty()->SetColor(colors->GetColor4d("Tomato").GetData());
  cylinderActor->RotateX(30.0);
  cylinderActor->RotateY(-45.0);

  renderer->AddActor(cylinderActor);
  renderer->SetBackground(colors->GetColor3d("BkgColor").GetData());
  renderer->ResetCamera();

  renWin->SetSize(WIDTH, HEIGHT);
  renWin->AddRenderer(renderer);
  renWin->SetNumberOfLayers(1);
  renWin->SetWindowName("Server");

  rwi->SetRenderWindow(renWin);
  rwi->Initialize();

  // corner annotation.
  renWin->SetNumberOfLayers(2);
  renderer2D->AddActor(cornerAnnotation);
  renderer2D->SetLayer(1);
  renderer2D->EraseOff();
  renderer2D->InteractiveOff();
  renWin->AddRenderer(renderer2D);
  cornerAnnotation->SetText("Stats (Server)");
  cornerAnnotation->SetRenderer(renderer2D);
  cornerAnnotation->BuildRepresentation();
  cornerAnnotation->SetWindowLocation(vtkTextRepresentation::UpperLeftCorner);
  cornerAnnotation->GetTextActor()->SetTextScaleModeToNone();
  cornerAnnotation->GetTextActor()->GetTextProperty()->SetBackgroundRGBA(0.1, 0.1, 0.1, 0.2);
  cornerAnnotation->SetShowBorder(true);
  cornerAnnotation->SetBorderColor(1., 0.86666667, 0.65098039);
  cornerAnnotation->SetCornerRadiusStrength(0.1);
  cornerAnnotation->SetPadding(4);
  cornerAnnotation->GetTextActor()->GetTextProperty()->SetFontFamilyToCourier();
  cornerAnnotation->GetTextActor()->GetTextProperty()->SetFontSize(24);
  cornerAnnotation->GetTextActor()->GetTextProperty()->SetJustificationToRight();
  cornerAnnotation->SetVisibility(true);
}

} // namespace

int main(int argc, char* argv[])
{
  // don't want to see vtk filter logs. do this first.
  ::SetupVTKPipeline();

  vtkLogger::SetThreadName(argv[0]);
  vtkLogger::SetStderrVerbosity(vtkLogger::VERBOSITY_9);
  if (argc < 2)
  {
    vtkLog(ERROR, << "Usage: " << argv[0] << "<Server port>" << endl);
    return EXIT_FAILURE;
  }

  vtkNew<vtkServerSocket> socket;
#if defined(_WIN32) && !defined(__CYGWIN__)
  WSAData wsaData;
  if (WSAStartup(WSA_VERSION, &wsaData))
  {
    vtkLog(ERROR, "Could not initialize sockets !");
  }
#endif
  vtkSocket* client = nullptr;
  vtkSmartPointer<vtkAbstractVideoEncoder> encoder = nullptr;
  if (argc > 2 && std::string(argv[2]) == "-intel")
  {
    encoder = vtk::TakeSmartPointer(vtkFFMPEGHardwareEncoder::New());
    vtkFFMPEGHardwareEncoder::SafeDownCast(encoder)->PreferIntelEncoders();
  }
  else if (argc > 2 && std::string(argv[2]) == "-nvidia")
  {
    encoder = vtk::TakeSmartPointer(vtkFFMPEGHardwareEncoder::New());
    vtkFFMPEGHardwareEncoder::SafeDownCast(encoder)->PreferNVIDIAEncoders();
  }
  else if (argc > 2 && std::string(argv[2]) == "-amd")
  {
    encoder = vtk::TakeSmartPointer(vtkFFMPEGHardwareEncoder::New());
    vtkFFMPEGHardwareEncoder::SafeDownCast(encoder)->PreferAMDEncoders();
  }
  else if (argc > 2 && std::string(argv[2]) == "-hw")
  {
    encoder = vtk::TakeSmartPointer(vtkFFMPEGHardwareEncoder::New());
  }
  else
  {
    encoder = vtk::TakeSmartPointer(vtkFFMPEGSoftwareEncoder::New());
  }
  encoder->SetCodec(ENCODERCODECTYPE);
  encoder->SetTimeBaseStart(1);
  encoder->SetTimeBaseEnd(MAXIMUM_FRAMES_ENCODED);
  encoder->SetBitRate(BIT_RATE);
  encoder->SetGroupOfPicturesSize(GOP_SIZE);
  vtkNew<vtkCallbackCommand> onEmitPacketCmd;
  onEmitPacketCmd->SetCallback(&::TransmitEncodedPacket);
  encoder->AddObserver(vtkCommand::ProgressEvent, onEmitPacketCmd);
  // webmWriter->SetForceNewClusters(true);
  // webmWriter->SetLiveMode(true);
  // webmWriter->SetOutputCues(false);
  // webmWriter->SetWriteToMemory(true);

  ::renWin->SetWindowName("Server");

  int serverPort = std::stoi(argv[1]);

  // creates, binds and puts the server socket in listen mode.
  socket->CreateServer(serverPort);
  vtkLog(INFO, << "Listening on port " << serverPort);

  std::chrono::high_resolution_clock::time_point lastIFrameT;
  std::chrono::high_resolution_clock::duration timeSinceIFrame;

  // run the event loop manually.
  while (!::rwi->GetDone())
  {
    // wait for client to connect. timeout in 1ms.
    if (client == nullptr)
    {
      vtkLog(TRACE, << "Waiting for a client to connect .. timeout 1ms");
      client = socket->WaitForConnection(1);
      if (client != nullptr)
      {
        vtkLog(INFO, << "A client connected.");
        // send initial camera to client.
        double state[18];
        vtk_helpers::SaveState(state, ::renderer);
        client->Send(state, sizeof(state));
        vtkLog(TRACE, << "Sent initial state");
        ::renWin->SetWindowName("Server");
      }
      else
      {
        if (timestamp > 1)
        {
          // webmWriter->WriteFileTrailer();
          std::cout << "Stats summary:\n";
          std::cout << "Total number of frames: " << totalNumFrames << '\n';
          std::cout << "Average Compression ratio: " << CRAvg / (double)totalNumFrames << '\n';
          std::cout << "Average GPU->CPU copy time: "
                    << nice_fmt::getNiceTime(CaptureAvgT / totalNumFrames) << '\n';
          std::cout << "Average Encode time: " << nice_fmt::getNiceTime(EncodeAvgT / totalNumFrames)
                    << '\n';
          std::cout << "Average Scale time: " << nice_fmt::getNiceTime(ScaleAvgT / totalNumFrames)
                    << '\n';
          std::cout << "Average Send time: " << nice_fmt::getNiceTime(SendAvgT / totalNumFrames)
                    << '\n';
          timestamp = 1;
          totalNumFrames = 0;
          CaptureAvgT = 0;
          EncodeAvgT = 0;
          ScaleAvgT = 0;
          SendAvgT = 0;
          CRAvg = 0;
        }
        ::rwi->ProcessEvents();
        std::string name = "Server (Waiting for client connection)";
        ::renWin->SetWindowName(name.c_str());
      }
    }

    // if client is connected, stream our rendering to them.
    if (client != nullptr && client->GetConnected())
    {
      // get camera from the client.
      double state[18];
      int recvSze = client->Receive(state, sizeof(state));
      if (recvSze == 0)
      {
        vtkLog(INFO, << "Client disconnected. Switch to wait mode..");
        client = nullptr;
        continue;
      }
      vtkLog(TRACE, << "Got state");
      vtk_helpers::LoadState(state, ::renderer);
      // force the clip planes to account for the actor bounds.
      ::renderer->ResetCameraClippingRange();

      // Render on server with the client's camera settings.
      ::rwi->Render();
      ::Capture(::renderer);

#if !USE_VTK_JPEG && !USE_VTK_PNG
      timeSinceIFrame = std::chrono::high_resolution_clock::now() - lastIFrameT;
      if (timeSinceIFrame.count() / 1e6 > IFRAME_MAXIMUM_INTERVAL_MSEC)
      {
        vtkLog(TRACE, << "I-Frame here.");
        lastIFrameT = std::chrono::high_resolution_clock::now();
        encoder->ForceIFrameOn();
      }
      else
      {
        vtkLog(TRACE, << "Use predictive/b-frame/i-frame based upon encoder gop_size.");
        encoder->ForceIFrameOff();
      }
#endif

      /* Ready to start encoding. Inform client. */
      int startMsgSize[1] = { 5 };
      client->Send(startMsgSize, sizeof(int));
      client->Send(startMsg, 5 * sizeof(char));

      /* Encode the frame into a packet. */
#if USE_VTK_JPEG
      t1JpegEnc = std::chrono::high_resolution_clock::now();
      jpegWriter->SetInputData(image);
      jpegWriter->WriteToMemoryOn();
      jpegWriter->SetQuality(QUALITY * 10);
      jpegWriter->Write();
      t2JpegEnc = std::chrono::high_resolution_clock::now();
      vtkUnsignedCharArray* packet = jpegWriter->GetResult();
      std::stringstream fname;
      fname << "frame-" << std::setfill('0') << std::setw(3) << totalNumFrames << ".jpeg";
      std::fstream file(fname.str(), file.binary | file.in | file.out | file.trunc);
      if (file.is_open() && packet)
      {
        file.write(reinterpret_cast<char*>(packet->GetPointer(0)), packet->GetNumberOfValues());
        file.close();
      }
      TransmitEncodedPacket(nullptr, 0, client, packet);
#elif USE_VTK_PNG
      t1PngEnc = std::chrono::high_resolution_clock::now();
      pngWriter->SetInputData(image);
      pngWriter->WriteToMemoryOn();
      pngWriter->SetCompressionLevel(QUALITY - 1);
      pngWriter->Write();
      t2PngEnc = std::chrono::high_resolution_clock::now();
      vtkUnsignedCharArray* packet = pngWriter->GetResult();
      TransmitEncodedPacket(nullptr, 0, client, packet);
#else
      // Begin the encoding on server.
      vtkLog(TRACE, << "Encoding " << timestamp);
      // Transmit each packet to the client.
      // onEmitPacketCmd->SetClientData(client); // set the client as the "clientData"
      bool success = encoder->Push(::videoFrame);
      if (!success)
      {
        vtkLog(WARNING, << "Failed to push frame for encoding");
        continue;
      }
      auto result = encoder->GetResult();
      if (result == nullptr)
      {
        vtkLog(ERROR, "Packet is null!")
      }
      if (result != nullptr)
      {
        ::TransmitEncodedPacket(encoder, vtkCommand::ProgressEvent, client, result);
      }
#endif
      /* Finished encoding. Inform client. */
      int finishMsgSize[1] = { 6 };
      client->Send(finishMsgSize, sizeof(int));
      client->Send(finishMsg, 6 * sizeof(char));
      ++timestamp;
      ++totalNumFrames;
      // if (totalNumFrames > 400)
      // {
      //   break;
      // }
      // let the encoder believe it's getting a constant number of frames.
      timestamp %= timestamp == encoder->GetTimeBaseEnd() ? encoder->GetTimeBaseEnd() + 1
                                                          : encoder->GetTimeBaseEnd();
    }
  }

  socket->CloseSocket();

  return EXIT_SUCCESS;
}
