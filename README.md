## Description
This project explores the idea of sending compressed frames from a vtk render window in one process to a client vtk render window in another process based on interaction in the client.
The novelty here is that the compression takes into account previous frames and possibly future frames. You will find that the example cpp client-server application fully fleshes out the implementation of controlling I-Frame and P-Frame(s) based on user interaction in the client window. By integrating closely with the ffmpeg C libraries, the application can instruct ffmpeg to encode a frame as an I-Frame and the rest as P-Frames. It is also possible to adjust the `gop_size` (size of group of pictures) variable. As far as client-server is concerned, it uses simple `vtkSocket`.

You may find benchmarks [here](https://gitlab.kitware.com/jaswant.panchumarti/streamvtkffmpegtest/-/blob/master/performance.md) interesting.

## Structure
An explaination of the directory structure:
```bash
├── attic                 # old prototype application
├── build.md              # build instructions for linux
├── CMake                 
│   └── FindFFMPEG.cmake  # Copied from VTK source tree.
├── CMakeLists.txt       
├── Examples              # Demo live encoding/decoding of vtk render window frames.
│   └── Cpp
│       ├── Client.cpp    # uses decoder
│       ├── include
│       │   ├── Common.h
│       │   └── Config.h  # configure window size, codec type and parameters here.
│       └── Server.cpp    # uses encoder
├── implementation.md     # notes
├── README.md
├── Video                 # Eventually, this directory will be moved as it is to VTK/
│   ├── FFMPEG
│   │   ├── CMakeLists.txt
│   │   ├── vtkFFMPEGDecoder.cxx
│   │   ├── vtkFFMPEGDecoder.h
│       ├── vtkFFMPEGEncoder.cxx
│       ├── vtkFFMPEGEncoder.h
│       └── vtk.module
```

## Configuration
The configuration of the example is done through `#define`(s) the `Examples/Cpp/include/Config.h` header file. Here is a brief listing of the parameters. Refer to that header for a detailed documentation.

Window size
```bash
WIDTH
HEIGHT
```
Encode Quality
```bash
QUALITY # 1: low, 10: high
BIT_RATE # Default 2Mbps, configure as needed depending on widthxheight
```

I-frame control
```bash 
# forces an i-frame if the time since previous I-Frame is larger than this number.
IFRAME_MAXIMUM_INTERVAL_MSEC
```

Define CODEC in `Examples/Cpp/include/Config.h`
```bash
Values -
1: HEVC/H.265
2: vp9
3: libopenjpeg
4: bitmap (BMP)
3: AV1
```

You can also use vtkJPEGReader/vtkJPEGWriter/vtkPNGReader/vtkPNGWriter by setting any one of these parameters to 1. Do not forget to turn off the other by setting it to 0. Only one of these can be active.
```bash
USE_VTK_JPEG
USE_VTK_PNG
```


## Usage
1. In a shell, run the server. `./server 8080`. Any port number is fine. You should be able to interact with the camera in this window. The title bar will read "Server (Waiting for client connection)"

2. Now, run the client in any other shell. `./client localhost 8080`. If you installed all the dependencies, you should see the client window appear. From this point on, the server interaction is disabled.
Feel free to move around the mouse in client, zoom, pan and observe the encode/decode/send/receive and cycle times. 

3. After you close the client, the server will go back to waiting mode. You can re-run the client.

Here is what you may see if everything went right.
![interactive-streaming](image.png)

[vp9-cpu.mp4](https://gitlab.kitware.com/jaswant.panchumarti/streamvtkffmpegtest/-/blob/master/recordings/vp9-cpu.mp4) shows an interactive demo of a client live streaming the server's renders with the VP9 cpu encoder and decoder. 
Both client and server execute on the same linux machine. 

It is interesting to note that although bmp-uncompressed has lossless quality, the throughput is very high ~ 5Mb, compared to Vp9 or x265 both of which have a high compression ratio 
The throughput for VP9 and x265 is in the order of kilobytes.

You can find more recordings [here](https://gitlab.kitware.com/jaswant.panchumarti/streamvtkffmpegtest/-/blob/master/recordings/)
