#include <chrono>
#include <cstdlib>
#include <string>

#include <vtkActor.h>
#include <vtkCallbackCommand.h>
#include <vtkCamera.h>
#include <vtkClientSocket.h>
#include <vtkCommand.h>
#include <vtkCylinderSource.h>
#include <vtkInteractorStyle.h>
#include <vtkLogger.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkOpenGLRenderWindow.h>
#include <vtkOpenGLState.h>
#include <vtkPointData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderTimerLog.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkUnsignedCharArray.h>
#include <vtk_glew.h>

#include "things.h"

namespace
{

// VTK objects.
vtkNew<vtkImageData> img;
vtkNew<vtkRenderer> renderer;
vtkNew<vtkRenderWindow> renWin;
vtkNew<vtkRenderWindowInteractor> rwi;
vtkNew<vtkClientSocket> socket;
vtkMTimeType LastCameraMTime = 0, LastRWMTime = 0;

// utils
int RWObserverId;
std::vector<int> RWIObserverIds;
std::chrono::high_resolution_clock::time_point lastDecodeT, lastReceiveT, lastCycleT;
std::chrono::duration<long, std::ratio<1, 1000000000>>::rep deltaReceiveTCount;

// FFMPEG objects.
AVCodecContext* ctx = NULL;
const AVCodec* codec = NULL;
struct SwsContext* sws_context = NULL;
AVFrame *rgba32Frame, *dstFrame;
AVPacket* pkt;

void PushImagetoViewport(vtkObject*, unsigned long, void*, void*)
{
  if (!rgba32Frame)
  {
    return;
  }
  renderer->Clear();
  int size[2], low_point[2];
  renderer->GetTiledSizeAndOrigin(&size[0], &size[1], &low_point[0], &low_point[1]);
  if (size[0] <= 0 || size[1] <= 0)
  {
    vtkGenericWarningMacro("Viewport empty. Cannot push to screen.");
    return;
  }

  vtkOpenGLState* ostate =
    static_cast<vtkOpenGLRenderWindow*>(renderer->GetVTKWindow())->GetState();
  ostate->vtkglEnable(GL_SCISSOR_TEST);
  ostate->vtkglViewport(low_point[0], low_point[1], size[0], size[1]);
  ostate->vtkglScissor(low_point[0], low_point[1], size[0], size[1]);

  // framebuffers have their color premultiplied by alpha.
  vtkOpenGLState::ScopedglBlendFuncSeparate bfsaver(ostate);
  vtkOpenGLState::ScopedglEnableDisable bsaver(ostate, GL_BLEND);
  if (true)
  {
    vtkLogF(TRACE, "PushToFrameBuffer: using blend");
    ostate->vtkglEnable(GL_BLEND);
    ostate->vtkglBlendFuncSeparate(GL_ONE, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
  }
  else
  {
    ostate->vtkglDisable(GL_BLEND);
    vtkLogF(TRACE, "PushToFrameBuffer: not-using blend");
  }
  vtkOpenGLRenderWindow* oglRenWin = vtkOpenGLRenderWindow::SafeDownCast(renWin);
  int width = renWin->GetSize()[0];
  int height = renWin->GetSize()[1];
  if (::rgba32Frame->width * ::rgba32Frame->height != width * height)
  {
    // resize messed up.
    return;
  }
  oglRenWin->DrawPixels(low_point[0], low_point[1], low_point[0] + size[0] - 1,
    low_point[1] + size[1] - 1, 0, 0, width - 1, height - 1, width, height, 4, VTK_UNSIGNED_CHAR,
    ::rgba32Frame->data[0]);
}

bool SetupDecoderFrame(int size[2])
{
  vtkLog(TRACE, << "Create a new rgba32 frame " << size[0] << 'x' << size[1]);

  // open the codec.
  int ret = avcodec_open2(ctx, codec, NULL);
  if (ret < 0)
  {
    vtkLog(ERROR, << "Could not open codec");
    return false;
  }

  // prepare an AVFrame. This will be the input to the decoder.
  // Eventually, we put the AVFrame data into our vtk renderer.
  rgba32Frame = av_frame_alloc();
  if (!rgba32Frame)
  {
    vtkLog(ERROR, << "Could not allocate video frame");
    return false;
  }

  rgba32Frame->format = AV_PIX_FMT_RGBA;
  rgba32Frame->width = size[0];
  rgba32Frame->height = size[1];

  ret = av_frame_get_buffer(rgba32Frame, 0);
  if (ret < 0)
  {
    vtkLog(ERROR, << "Could not allocate the video frame data");
    return false;
  }

  return true;
}

void TearDownDecoderFrame()
{
  if (rgba32Frame == NULL)
  {
    return;
  }
  av_frame_free(&rgba32Frame);
  rgba32Frame = NULL;
}

bool UpdateCameraIfNeeded()
{
  if (LastCameraMTime >= renderer->GetActiveCamera()->GetMTime())
  {
    return false;
  }

  if (vtk_helpers::SizeChanged(renWin->GetSize()))
  {
    ::TearDownDecoderFrame();
    if (!::SetupDecoderFrame(renWin->GetSize()))
    {
      vtkLog(ERROR, "Failed to setup decoder frame.");
      return false;
    }
  }

  // push our camera to the server.
  double camera[16];
  vtk_helpers::SaveCamera(camera, renderer);
  socket->Send(camera, sizeof(camera));
  vtkLog(TRACE, << "Send camera");
  vtkLog(TRACE, << "--------------------------------");

  return true;
}

void HandleInteractorEvent(vtkObject*, unsigned long ev, void*, void*)
{
  bool sent = false;
  switch (ev)
  {
    case vtkCommand::RenderEvent:
    case vtkCommand::WindowResizeEvent:
    case vtkCommand::ConfigureEvent:
      sent = UpdateCameraIfNeeded();
      break;
  }
  vtk_helpers::UpdateLastSize(renWin->GetSize());

  if (!sent)
  {
    return;
  }

  bool start = false;
  bool finished = false;
  /* Look for magic start message */
  char startBuf[5] = {};
  char finishBuf[6] = {};
  do
  {
    int pktSize = 0;
    int recvMsgSize = socket->Receive(&pktSize, sizeof(pktSize));
    {
      vtkLog(TRACE, << "recvMsgSize " << recvMsgSize);
      vtkLog(TRACE, << "pktSize " << pktSize);
    }
    switch (pktSize)
    {
      case 5:
        socket->Receive(startBuf, pktSize);
        if (std::string(startBuf) == startMsg)
        {
          vtkLog(TRACE, << "Received start message");
          start = true;
          continue;
        }
      case 6:
        socket->Receive(finishBuf, pktSize);
        if (std::string(finishBuf) == finishMsg)
        {
          vtkLog(TRACE, << "Received finish message");
          finished = true;
          break;
        }
      default:
        void* recvBuffer = av_malloc(pktSize);
        lastReceiveT = std::chrono::high_resolution_clock::now();
        recvMsgSize = socket->Receive(recvBuffer, pktSize);
        auto now = std::chrono::high_resolution_clock::now();
        deltaReceiveTCount = (now - lastReceiveT).count();
        lastReceiveT = now;
        vtkLog(TRACE, << "Receive " << deltaReceiveTCount / 1e6 << "ms");
        int ret = av_packet_from_data(pkt, reinterpret_cast<uint8_t*>(recvBuffer), pktSize);
        if (ret == AVERROR(EINVAL))
        {
          vtkLog(ERROR, "Invalid packet size. " << pktSize);
          finished = true;
        }
        else if (ret == AVERROR(ENOMEM))
        {
          vtkLog(ERROR, "Out of memory. Requested " << pktSize << "Bytes") finished = true;
        }
        vtkLog(TRACE, << "Received packet " << recvMsgSize);
        if (recvMsgSize == 0)
        {
          finished = true;
        }
        break;
    }
    if (start && !finished)
    {
      int* size = renWin->GetSize();

      auto frameHandler = [](AVFrame* frame)
      {
        vtkLog(TRACE, << "Decoded " << ctx->frame_number);
        auto now = std::chrono::high_resolution_clock::now();
        auto deltaDecodeT = now - lastDecodeT;
        vtkLog(TRACE, << "Decode " << int(deltaDecodeT.count() / 1e6) << "ms");

        ffmpeg::utils::set_frame_rgba32_from_decoded_frame(frame, sws_context, rgba32Frame);
        now = std::chrono::high_resolution_clock::now();

        std::stringstream sst;
        sst << "cycletime " << int((now - lastCycleT).count() / 1e6) << "ms|";
        sst << "decode " << int(deltaDecodeT.count() / 1e6) << "ms|";
        sst << "receive " << int(deltaReceiveTCount / 1e3) << "us";
        std::string name = "Client " + sst.str();

        renWin->SetWindowName(name.c_str());
        renWin->Render();
        now = std::chrono::high_resolution_clock::now();
        lastCycleT = now;
      };
      lastDecodeT = std::chrono::high_resolution_clock::now();
      ffmpeg::decoding::decode(ctx, dstFrame, pkt, frameHandler);
      av_packet_unref(pkt);
    }
  } while (!finished);
}

void SetupInteractors(vtkObject*, unsigned long ev, void*, void*)
{
  auto events = { vtkCommand::RenderEvent, vtkCommand::WindowResizeEvent,
    vtkCommand::ConfigureEvent };
  for (const auto& ev : events)
  {
    vtkNew<vtkCallbackCommand> cmd;
    cmd->SetCallback(&HandleInteractorEvent);
    RWIObserverIds.push_back(rwi->AddObserver(ev, cmd));
  }
  // Remove the observer so we don't end up calling this method again.
  renWin->RemoveObserver(RWObserverId);
  RWObserverId = 0;
}

void SetupRenderWindow()
{
  // The render window is the actual GUI window
  // that appears on the computer screen
  renWin->SetSize(WIDTH, HEIGHT);
  renWin->AddRenderer(renderer);
  renWin->SetWindowName("Client");

  // The render window interactor captures mouse events
  // and will perform appropriate camera or actor manipulation
  // depending on the nature of the events.
  rwi->SetRenderWindow(renWin);
  rwi->Initialize();
  vtkNew<vtkCallbackCommand> cmd;
  cmd->SetCallback(&PushImagetoViewport);
  renderer->AddObserver(vtkCommand::EndEvent, cmd);

  {
    vtkNew<vtkCallbackCommand> cmd;
    cmd->SetCallback(&SetupInteractors);
    RWObserverId = renWin->AddObserver(vtkCommand::ModifiedEvent, cmd);
  }
}

} // namespace

int main(int argc, char* argv[])
{
  if (argc < 3)
  { // Test for correct number of arguments
    vtkLog(ERROR, << "Usage: " << argv[0] << " <Server> <Server Port>\n");
    exit(1);
  }

  std::string serverAddress = argv[1];
  int serverPort = std::stoi(argv[2]);
  // vtkLogger::SetStderrVerbosity(vtkLogger::VERBOSITY_9);

  if (socket->ConnectToServer(serverAddress.c_str(), serverPort) == -1)
  {
    vtkLog(ERROR, << "Failed to connect " << serverAddress << ':' << serverPort);
    return EXIT_FAILURE;
  }
  else
  {
    vtkLog(INFO, << "Connected to " << serverAddress << ':' << serverPort);
    // get initial camera from server.
    double camera[16];
    socket->Receive(camera, sizeof(camera));
    vtkLog(TRACE, << "Got initial camera");
    vtkLog(TRACE, << "--------------------------------");
    vtk_helpers::LoadCamera(camera, renderer);
  }

  /* Prepare decoder */
#ifndef NDEBUG
  av_log_set_level(AV_LOG_VERBOSE);
#endif

  pkt = av_packet_alloc();
  if (!pkt)
  {
    vtkLog(ERROR, << "Failed to allocate a packet");
    return EXIT_FAILURE;
  }

  codec = avcodec_find_decoder_by_name(FFMPEG_DECODER);
  if (!codec)
  {
    vtkLogF(ERROR, "Codec %s not found", FFMPEG_DECODER);
    return EXIT_FAILURE;
  }
  vtkLog(INFO, << "Codec description");
  vtkLog(INFO, << "Name " << codec->name);
  vtkLog(INFO, << "Long name " << codec->long_name);
  vtkLog(INFO, << "Wrapper name " << codec->wrapper_name);

  ctx = avcodec_alloc_context3(codec);
  if (!ctx)
  {
    vtkLog(ERROR, << "Could not allocate video codec context");
    return EXIT_FAILURE;
  }
  /* open it */
  if (avcodec_open2(ctx, codec, NULL) < 0)
  {
    vtkLog(ERROR, "Could not open codec");
    return EXIT_FAILURE;
  }

  dstFrame = av_frame_alloc();
  if (!dstFrame)
  {
    vtkLog(ERROR, "Could not allocate dst frame");
    return EXIT_FAILURE;
  }

  /* Prepare and start rendering */
  ::SetupRenderWindow();
  // force an post-initial push-pull. this makes sure the windows displays an
  // image from the server upon first show
  ::renWin->InvokeEvent(vtkCommand::ModifiedEvent);
  ::rwi->Render();
  ::rwi->Start();

  /* Cleanup observers */
  renderer->RemoveObserver(vtkCommand::EndEvent);
  for (auto oid : RWIObserverIds)
  {
    rwi->RemoveObserver(oid);
  }

  /* Cleanup decoder */
  avcodec_free_context(&ctx);
  ::TearDownDecoderFrame();
  av_packet_free(&pkt);
  socket->CloseSocket();
  return EXIT_SUCCESS;
}
