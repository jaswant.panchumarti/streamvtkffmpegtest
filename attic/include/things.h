#pragma once
#include <cstdint>
#include <vtkBMPWriter.h>
#include <vtkCamera.h>
#include <vtkImageData.h>
#include <vtkLogger.h>
#include <vtkRenderer.h>
#include <vtkUnsignedCharArray.h>

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavutil/error.h>
#include <libavutil/frame.h>
#include <libavutil/imgutils.h>
#include <libavutil/opt.h>
#include <libavutil/parseutils.h>
#include <libavutil/pixfmt.h>
#include <libswscale/swscale.h>
}

#define USE_VP9 0   // google's vp9
#define USE_HEVC 1  // x265
#define USE_NVENC 0 // If you've nvidia gpu, enable this.

#if USE_VP9
#define FFMPEG_FRAME_FMT AV_PIX_FMT_YUV420P
#define FFMPEG_ENCODER "libvpx-vp9"
#define FFMPEG_DECODER "libvpx-vp9"
#elif USE_HEVC
#if USE_NVENC
#define FFMPEG_FRAME_FMT AV_PIX_FMT_NV12
#define FFMPEG_ENCODER "hevc_nvenc"
#define FFMPEG_DECODER "hevc_cuvid"
#else
#define FFMPEG_FRAME_FMT AV_PIX_FMT_YUV420P
#define FFMPEG_ENCODER "libx265"
#define FFMPEG_DECODER "hevc"
#endif
#endif
#define WIDTH 1920
#define HEIGHT 1080
#define BIT_RATE 2000000 // 2Mbps

/* The encoder will enforce an I-Frame if the time b/w consecutive renders is larger than this
 * number */
#define IFRAME_MINIMUM_INTERVAL_MSEC 100
/* The encoder will enforce an I-Frame if the time since previous I-Frame is larger than this
 * number */
#define IFRAME_MAXIMUM_INTERVAL_MSEC 2000
/* The encoder shall accept a maximum number of frames after which it shall return EOF */
#define MAXIMUM_FRAMES_ENCODED 20
/* The encoder will do it's thing over group of pictures for this size */
#define GOP_SIZE 10

static const char* startMsg = "start";
static const char* finishMsg = "finish";
static int LastSize[2] = { -1, -1 };

namespace vtk_helpers
{

inline void UpdateLastSize(int size[2])
{
  LastSize[0] = size[0];
  LastSize[1] = size[1];
}

inline bool SizeChanged(int size[2])
{
  return (LastSize[0] != size[0]) || (LastSize[1] != size[1]);
}

inline void SaveCamera(double camState[16], vtkRenderer* renderer)
{
  vtkCamera* cam = renderer->GetActiveCamera();
  cam->GetPosition(camState);                  // 3
  cam->GetFocalPoint(&camState[3]);            // 3
  cam->GetViewUp(&camState[6]);                // 3
  cam->GetWindowCenter(&camState[9]);          // 2
  cam->GetClippingRange(&camState[11]);        // 2
  camState[13] = cam->GetViewAngle();          // 1
  camState[14] = cam->GetParallelScale();      // 1
  camState[15] = cam->GetParallelProjection(); // 1
}

inline void LoadCamera(double camState[16], vtkRenderer* renderer)
{
  vtkCamera* cam = renderer->GetActiveCamera();
  cam->SetPosition(camState);                      // 3
  cam->SetFocalPoint(&camState[3]);                // 3
  cam->SetViewUp(&camState[6]);                    // 3
  cam->SetWindowCenter(camState[9], camState[10]); // 2
  // cam->SetClippingRange(&camState[11]);            // 2
  // cam->SetViewAngle(camState[13]);                 // 1
  // cam->SetParallelScale(camState[14]);             // 1
  cam->SetParallelProjection(camState[15] == 1.0); // 1
}

inline void PrintCameraState(double camState[16])
{
  cout << "Position : " << camState[0] << ' ' << camState[1] << ' ' << camState[2] << '\n';
  cout << "FocalPoint : " << camState[3] << ' ' << camState[4] << ' ' << camState[5] << '\n';
  cout << "ViewUp : " << camState[6] << ' ' << camState[7] << ' ' << camState[8] << '\n';
  cout << "WindowCenter : " << camState[9] << ' ' << camState[10] << '\n';
  cout << "ClippingRange : " << camState[11] << ' ' << camState[12] << '\n';
  cout << "ViewAngle : " << camState[13] << '\n';
  cout << "ParallelScale : " << camState[14] << '\n';
  cout << "ParallelProjection : " << camState[15] << '\n';
}

inline void SaveImage(int w, int h, vtkUnsignedCharArray* data, const char* fname)
{
  vtkNew<vtkImageData> img;
  img->SetDimensions(w, h, 1);
  img->AllocateScalars(data->GetDataType(), data->GetNumberOfComponents());
  memcpy(
    img->GetScalarPointer(), data->GetPointer(0), data->GetDataSize() * data->GetDataTypeSize());
  vtkNew<vtkBMPWriter> writer;
  writer->SetInputData(img);
  writer->SetFileName(fname);
  writer->Write();
}

}
namespace ffmpeg
{
namespace utils
{

/**
 * Initializes the frame pixel format, width, height and allocates frame data and linesize pointers.
 * returns false if any step failed.
 */
inline bool init_frame(AVFrame** frame_ptr, int w, int h, AVCodecContext* ctx)
{
  int ret;
  AVFrame* frame;
  frame = av_frame_alloc();
  if (!frame)
  {
    vtkLog(ERROR, << "Could not allocate video frame");
    return false;
  }

  frame->format = ctx->pix_fmt;
  frame->width = w;
  frame->height = h;
  ret = av_image_alloc(
    frame->data, frame->linesize, frame->width, frame->height, (AVPixelFormat)frame->format, 32);
  if (ret < 0)
  {
    vtkLog(ERROR, << "Could not allocate raw picture buffer");
    return false;
  }
  *frame_ptr = frame;
  return true;
}

/**
 * Converts the input data in rgba32 pixel format to a frame for encoding.
 */
inline bool set_encoder_frame_from_rgba32(
  std::uint8_t* rgba32, struct SwsContext* sws_context, AVFrame* frame)
{
  sws_context = sws_getCachedContext(sws_context, frame->width, frame->height, AV_PIX_FMT_RGBA,
    frame->width, frame->height, FFMPEG_FRAME_FMT, 0, 0, 0, 0);
  if (!sws_context)
  {
    vtkLog(ERROR, << "Could not initialize a scaling context for given parameters");
    return false;
  }
  // r, g, b, a -> four components, total size = 4 * w * h, linesize = total_size/h
  const int in_linesize[1] = { 4 * frame->width };
  sws_scale(sws_context, (const uint8_t* const*)&rgba32, in_linesize, 0, frame->height, frame->data,
    frame->linesize);
  return true;
}

/**
 * Converts the input frame to an rgba32 pixel format frame.
 */
inline bool set_frame_rgba32_from_decoded_frame(
  AVFrame* src_frame, struct SwsContext* sws_context, AVFrame* dst_frame)
{
  sws_context = sws_getCachedContext(sws_context, src_frame->width, src_frame->height,
    FFMPEG_FRAME_FMT, dst_frame->width, dst_frame->height, AV_PIX_FMT_RGBA, 0, 0, 0, 0);
  if (!sws_context)
  {
    vtkLog(ERROR, << "Could not initialize a scaling context for given parameters");
    return false;
  }
  sws_scale(sws_context, (const uint8_t* const*)src_frame->data, src_frame->linesize, 0,
    src_frame->height, dst_frame->data, dst_frame->linesize);
  return true;
}
} // end utils

namespace decoding
{
template <typename FrameHandlerT>
inline bool decode(
  AVCodecContext* dec_ctx, AVFrame* frame, AVPacket* pkt, FrameHandlerT&& frame_handler)
{
  int ret;

  ret = avcodec_send_packet(dec_ctx, pkt);
  if (ret < 0)
  {
    vtkLog(ERROR, "Error sending a packet for decoding");
    return false;
  }

  while (ret >= 0)
  {
    ret = avcodec_receive_frame(dec_ctx, frame);
    switch (ret)
    {
      case AVERROR(EAGAIN):
      {
        vtkLog(TRACE, << "Decoder needs more input packets");
        return true;
      }
      case AVERROR_EOF:
      {
        vtkLog(TRACE, << "Decoder input is flushed. No more output frames.");
        return true;
      }
      case AVERROR(EINVAL):
      {
        vtkLog(ERROR, << "Codec is not opened, or the context is a decoding context.");
        break;
      }
      default:
        break;
    }
    if (ret < 0)
    {
      vtkLog(ERROR, "Error during encoding");
      return false;
    }
    else
    {
      frame_handler(frame);
    }
  }
  return true;
}
}

namespace encoding
{

template <typename PacketHandlerT>
inline bool encode(
  AVCodecContext* enc_ctx, AVFrame* frame, AVPacket* pkt, PacketHandlerT&& packet_handler)
{
  int ret;
  if (frame)
  {
    vtkLog(TRACE, << "Send frame " << frame->pts);
  }
  else
  {
    vtkLog(TRACE, << "Drain the encoder");
  }

  ret = avcodec_send_frame(enc_ctx, frame);
  if (ret < 0)
  {
    vtkLog(ERROR, << "Error sending a frame for encoding");
    return false;
  }

  /* Receive output in a loop. */
  // Continue to get as many packets as possible from the encoder
  // until it gives AVERROR(EAGAIN) or AVERROR_EOF
  while (ret >= 0)
  {
    ret = avcodec_receive_packet(enc_ctx, pkt);
    switch (ret)
    {
      case AVERROR(EAGAIN):
      {
        vtkLog(TRACE, << "Encoder needs more input frames. Current frame->pts=" << frame->pts);
        return true;
      }
      case AVERROR_EOF:
      {
        vtkLog(TRACE, << "Encoder input is flushed. No more output packets.");
        return true;
      }
      case AVERROR(EINVAL):
      {
        vtkLog(ERROR, << "Codec is not opened, or the context is a decoding context.");
        break;
      }
      default:
        break;
    }

    if (ret < 0)
    {
      vtkLog(ERROR, "Error during encoding");
      return false;
    }
    else
    {
      packet_handler(pkt);
    }
  }
  return true;
}

template <typename PacketHandlerT>
inline bool encode_rgba32_frame(std::uint8_t* rgba32, AVFrame* frame, AVCodecContext* ctx,
  struct SwsContext* sws_context, AVPacket* pkt, PacketHandlerT&& packet_handler)
{
  int ret;
  bool convertSuccess = ffmpeg::utils::set_encoder_frame_from_rgba32(rgba32, sws_context, frame);
  if (!convertSuccess)
  {
    vtkLog(ERROR, << "Failed to convert rgba32 to encoder frame");
    return false;
  }

  if (!frame)
  {
    vtkLog(ERROR, << "Frame is null!");
    return false;
  }
  ret = av_frame_make_writable(frame);
  if (ret < 0)
  {
    vtkLog(ERROR, "Failed to make frame writable");
  }

  return ffmpeg::encoding::encode(ctx, frame, pkt, packet_handler);
}

inline bool finish()
{
  return true;
}
} // end encoding
}
