#include <array>
#include <chrono>
#include <cstdlib>
#include <sstream>
#include <string>

#include <vtkActor.h>
#include <vtkCamera.h>
#include <vtkClientSocket.h>
#include <vtkCylinderSource.h>
#include <vtkLogger.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkPointData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkServerSocket.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>
#include <vtkTextRepresentation.h>
#include <vtkType.h>
#include <vtkUnsignedCharArray.h>

#include "things.h"

namespace
{

// VTK objects.
vtkNew<vtkUnsignedCharArray> RGBA32Data;
vtkNew<vtkRenderer> renderer;
vtkNew<vtkRenderer> renderer2D;
vtkNew<vtkTextRepresentation> cornerAnnotation;
vtkNew<vtkRenderWindow> renWin;
vtkNew<vtkRenderWindowInteractor> rwi;
std::chrono::high_resolution_clock::time_point lastEncodeT, lastSendT;

// FFMPEG objects.
AVCodecContext* ctx = NULL;
const AVCodec* codec = NULL;
struct SwsContext* sws_context = NULL;
AVFrame* frame;
AVPacket* pkt;

// copied from
// VTK/Rendering/Parallel/vtkSynchronizedRenderers::vtkRawImage::Capture with
// one modification - removed this pointer acccess
void Capture(vtkRenderer* ren)
{
  double viewport[4];
  ren->GetViewport(viewport);

  int window_size[2];
  window_size[0] = ren->GetVTKWindow()->GetActualSize()[0];
  window_size[1] = ren->GetVTKWindow()->GetActualSize()[1];

  int viewport_in_pixels[4];
  viewport_in_pixels[0] = static_cast<int>(window_size[0] * viewport[0]);
  viewport_in_pixels[1] = static_cast<int>(window_size[1] * viewport[1]);
  viewport_in_pixels[2] = static_cast<int>(window_size[0] * viewport[2]) - 1;
  viewport_in_pixels[3] = static_cast<int>(window_size[1] * viewport[3]) - 1;

  // we need to ensure that the size computation is always done in pixels,
  // otherwise we end up with rounding issues. In short, avoid doing
  // additions/subtractions using normalized viewport coordinates. Those are
  // better done in pixels.
  int image_size[2];
  image_size[0] = viewport_in_pixels[2] - viewport_in_pixels[0] + 1;
  image_size[1] = viewport_in_pixels[3] - viewport_in_pixels[1] + 1;

  // using RGBA always?
  // this->Resize(image_size[0], image_size[1], 4);
  ::RGBA32Data->SetNumberOfComponents(4);
  ::RGBA32Data->SetNumberOfTuples(image_size[0] * image_size[1]);

  ren->GetRenderWindow()->GetRGBACharPixelData(viewport_in_pixels[0], viewport_in_pixels[1],
    viewport_in_pixels[2], viewport_in_pixels[3], 0, ::RGBA32Data,
    /*right=*/ren->GetActiveCamera()->GetLeftEye() == 0);
}

// https://kitware.github.io/vtk-examples/site/Cxx/GeometricObjects/CylinderExample/
void SetupVTKPipeline()
{
  vtkNew<vtkNamedColors> colors;

  std::array<unsigned char, 4> bkg{ { 26, 51, 102, 255 } };
  colors->SetColor("BkgColor", bkg.data());

  vtkNew<vtkCylinderSource> cylinder;
  cylinder->SetResolution(8);

  vtkNew<vtkPolyDataMapper> cylinderMapper;
  cylinderMapper->SetInputConnection(cylinder->GetOutputPort());

  vtkNew<vtkActor> cylinderActor;
  cylinderActor->SetMapper(cylinderMapper);
  cylinderActor->GetProperty()->SetColor(colors->GetColor4d("Tomato").GetData());
  cylinderActor->RotateX(30.0);
  cylinderActor->RotateY(-45.0);

  renderer->AddActor(cylinderActor);
  renderer->SetBackground(colors->GetColor3d("BkgColor").GetData());
  renderer->ResetCamera();

  renWin->SetSize(WIDTH, HEIGHT);
  renWin->AddRenderer(renderer);
  renWin->SetNumberOfLayers(1);
  renWin->AddRenderer(renderer2D);
  renWin->SetWindowName("Server");

  rwi->SetRenderWindow(renWin);
  rwi->Initialize();
  rwi->Render();

  // corner annotation.
  cornerAnnotation->SetText("Stats");
  cornerAnnotation->SetRenderer(renderer2D);
  cornerAnnotation->BuildRepresentation();
  cornerAnnotation->SetWindowLocation(vtkTextRepresentation::UpperLeftCorner);
  cornerAnnotation->GetTextActor()->SetTextScaleModeToNone();
  cornerAnnotation->GetTextActor()->GetTextProperty()->SetJustificationToLeft();
  cornerAnnotation->SetVisibility(true);
  renWin->SetNumberOfLayers(2);
  renderer2D->AddActor(cornerAnnotation);
  renderer2D->SetLayer(1);
  renderer2D->EraseOff();
  renderer2D->InteractiveOff();
  renWin->AddRenderer(renderer2D);
}

bool SetupEncoderFrame(int size[2])
{
  vtkLog(INFO, << "Create a new encoder frame " << size[0] << "x" << size[1]);
  ctx->bit_rate = BIT_RATE; // needs some experimenting.
  ctx->width = size[0];
  ctx->height = size[1];
  // encoder will make sense of these numbers.
  ctx->time_base = (AVRational){ 1, MAXIMUM_FRAMES_ENCODED };
  ctx->framerate = (AVRational){ MAXIMUM_FRAMES_ENCODED, 1 };

  /* emit one intra frame every GOP_SIZE number of frames
   * check frame pict_type before passing frame
   * to encoder, if frame->pict_type is AV_PICTURE_TYPE_I
   * then gop_size is ignored and the output of encoder
   * will always be I frame irrespective to gop_size
   */
  ctx->gop_size = GOP_SIZE;
  ctx->max_b_frames = 0; // bi-directional frames.
  ctx->pix_fmt = FFMPEG_FRAME_FMT;

  if (codec->id == AV_CODEC_ID_VP9)
  {
    // https://www.reddit.com/r/AV1/comments/k7colv/encoder_tuning_part_1_tuning_libvpxvp9_be_more/
    av_opt_set(ctx->priv_data, "lag-in-frames", "0", 0);
    av_opt_set(ctx->priv_data, "rc_lookahead", "0", 0); // bidirectional frames are disabled.
    av_opt_set(ctx->priv_data, "quality", "realtime", 0);
    av_opt_set(ctx->priv_data, "speed", "8", 0); // 5: high-quality, 8: low-quality
    av_opt_set(ctx->priv_data, "frame-boost", "1", 0);
    av_opt_set(ctx->priv_data, "tune-content", "screen", 0); // for live-streaming the renders
    av_opt_set(ctx->priv_data, "deadline", "realtime", 0);   // default is good
    av_opt_set(ctx->priv_data, "row-mt", "1", 0); // apparently, libvpx-vp9 disables this.
    av_opt_set(ctx->priv_data, "speed", "16", 0); // default is 1
  }
  else if (std::string(codec->name) == "hevc_nvenc")
  { // ffmpeg -h encoder=hevc_nvenc
    av_opt_set(ctx->priv_data, "rc", "cbr", 0);
    av_opt_set_int(ctx->priv_data, "rc-lookahead", 0, 0); // doesn't work. theres is some delay.
    av_opt_set(ctx->priv_data, "zerolatency", "0", 0);
    av_opt_set(ctx->priv_data, "tune", "ull", 0);
    av_opt_set(ctx->priv_data, "preset", "p1", 0);
  }
  else if (std::string(codec->name) == "libx265")
  {
    // https://trac.ffmpeg.org/wiki/Encode/H.264#crf
    // Applies well to libx265.
    av_opt_set(ctx->priv_data, "tune", "zerolatency", 0);
    av_opt_set(ctx->priv_data, "preset", "ultrafast", 0);
  }

  if (codec->id == AV_CODEC_ID_VP8)
  {
    // av_opt_set(ctx->priv_data, "auto-alt-ref", "0", 0);
    // av_opt_set(ctx->priv_data, "preset", )
  }

  // open the codec.
  int ret = avcodec_open2(ctx, codec, NULL);
  if (ret < 0)
  {
    vtkLog(ERROR, << "Could not open codec");
    return false;
  }

  // prepare an AVFrame. This will be the input to the encoder.
  // Eventually, we put our vtk render window's screenshot into the AVFrame object.
  frame = av_frame_alloc();
  if (!frame)
  {
    vtkLog(ERROR, << "Could not allocate video frame");
    return false;
  }
  frame->format = ctx->pix_fmt;
  frame->width = ctx->width;
  frame->height = ctx->height;

  ret = av_frame_get_buffer(frame, 0);
  if (ret < 0)
  {
    vtkLog(ERROR, << "Could not allocate the video frame data");
    return false;
  }

  return true;
}

void TearDownEncoderFrame()
{
  if (frame == NULL)
  {
    return;
  }
  av_frame_free(&frame);
  frame = NULL;
}

} // namespace

int main(int argc, char* argv[])
{
  vtkLogger::SetThreadName(argv[0]);

  if (argc < 2)
  {
    vtkLog(ERROR, << "Usage: " << argv[0] << "<Server port>" << endl);
    return EXIT_FAILURE;
  }

#ifndef NDEBUG
  av_log_set_level(AV_LOG_DEBUG);
#endif

  ::SetupVTKPipeline();

  int serverPort = std::stoi(argv[1]);
  // vtkLogger::SetStderrVerbosity(vtkLogger::VERBOSITY_9);

  vtkNew<vtkServerSocket> socket;
  // creates, binds and puts the server socket in listen mode.
  socket->CreateServer(serverPort);
  vtkLog(INFO, << "Listening on port " << serverPort);

  auto lastCycle = std::chrono::high_resolution_clock::now();
  vtkClientSocket* clientSock = nullptr;

  codec = avcodec_find_encoder_by_name(FFMPEG_ENCODER);
  if (!codec)
  {
    vtkLogF(ERROR, "Codec %s not found", FFMPEG_DECODER);
    return EXIT_FAILURE;
  }
  vtkLog(INFO, << "Codec description");
  vtkLog(INFO, << "Name " << codec->name);
  vtkLog(INFO, << "Long name " << codec->long_name);
  vtkLog(INFO, << "Wrapper name " << codec->wrapper_name);

  ctx = avcodec_alloc_context3(codec);
  if (!ctx)
  {
    vtkLog(ERROR, << "Could not allocate video codec context");
    return EXIT_FAILURE;
  }

  pkt = av_packet_alloc();
  if (!pkt)
  {
    return EXIT_FAILURE;
  }

  auto timeSinceIFrame = std::chrono::high_resolution_clock::now() - lastCycle;

  // run the event loop manually.
  while (!::rwi->GetDone())
  {
    // wait for client to connect. timeout in 1ms.
    if (clientSock == nullptr)
    {
      vtkLog(TRACE, << "Waiting for connection");
      clientSock = socket->WaitForConnection(1);

      if (clientSock != nullptr)
      {
        vtkLog(INFO, << "A client connected.");
        // send initial camera to client.
        double camera[16];
        vtk_helpers::SaveCamera(camera, ::renderer);
        clientSock->Send(camera, sizeof(camera));
        vtkLog(TRACE, << "Send initial camera");
        vtkLog(TRACE, << "--------------------------------");
      }
      else
      {
        ::rwi->ProcessEvents();
        const auto now = std::chrono::high_resolution_clock::now();
        std::string name = "Server (Waiting for client connection)";
        std::stringstream sst;
        sst << "stats\n";
        sst << "cycletime " << (now - lastCycle).count() / 1e6 << "ms";
        lastCycle = now;
        cornerAnnotation->SetText(sst.str().c_str());
        ::renWin->SetWindowName(name.c_str());
      }
    }

    // if client is connected, stream our rendering to them.
    if (clientSock != nullptr && clientSock->GetConnected())
    {
      // Get the width x height of our window.
      int size[2] = { renWin->GetSize()[0], renWin->GetSize()[1] };

      // get camera from the client.
      double camera[16];
      int recvSze = clientSock->Receive(camera, sizeof(camera));
      if (recvSze == 0)
      {
        vtkLog(INFO, << "Client disconnected. Switch to wait mode..");
        clientSock = nullptr;
        continue;
      }

      vtkLog(TRACE, << "Got camera");
      vtkLog(TRACE, << "--------------------------------");
      vtk_helpers::LoadCamera(camera, ::renderer);
      // force the clip planes to account for the actor bounds.
      ::renderer->ResetCameraClippingRange();

      // Render on server with the client's camera settings.
      ::rwi->Render();
      ::Capture(::renderer);
      const auto now = std::chrono::high_resolution_clock::now();
      const auto deltaT = now - lastCycle;
      const auto msecDelta = deltaT.count() / 1000000;
      std::string name = "Server cycletime " + std::to_string(msecDelta) + "msec";
      lastCycle = now;
      ::renWin->SetWindowName(name.c_str());

      // Reset or create a new encoder.
      if (vtk_helpers::SizeChanged(size))
      {
        ::TearDownEncoderFrame();
        if (!::SetupEncoderFrame(size))
        {
          vtkLog(ERROR, << "Failed to setup encoder frame.");
          vtk_helpers::UpdateLastSize(size);
          continue;
        }
        frame->pts = 0;
      }
      if (msecDelta > IFRAME_MINIMUM_INTERVAL_MSEC ||
        timeSinceIFrame.count() / 1e6 > IFRAME_MAXIMUM_INTERVAL_MSEC)
      {
        timeSinceIFrame -= timeSinceIFrame; // reset
        vtkLog(TRACE, << "I-Frame here.");
        frame->pict_type = AV_PICTURE_TYPE_I;
      }
      else
      {
        timeSinceIFrame += deltaT;
        vtkLog(TRACE, << "Use predictive/b-frame/i-frame based upon encoder gop_size.");
      }
      int ret = av_frame_make_writable(frame);
      if (ret < 0)
      {
        vtkLog(WARNING, << "Failed to make frame writable");
        vtk_helpers::UpdateLastSize(size);
        continue;
      }
      vtk_helpers::UpdateLastSize(size);

      /* Ready to start encoding. Inform client. */
      int startMsgSize[1] = { 5 };
      clientSock->Send(startMsgSize, sizeof(int));
      clientSock->Send(startMsg, strlen(startMsg) * sizeof(char));

      /* Encode the frame into a packet. */
      // let the encoder believe it's getting a constant number of frames.
      frame->pts %= MAXIMUM_FRAMES_ENCODED;
      // Intercept and transmit each packet to the client.
      auto packetHandler = [&clientSock](AVPacket* pkt)
      {
        auto now = std::chrono::high_resolution_clock::now();
        auto deltaEncode = now - ::lastEncodeT;
        vtkLog(TRACE, << "Encode " << deltaEncode.count() / 1e6 << "ms");

        lastSendT = std::chrono::high_resolution_clock::now();
        vtkLog(TRACE, << "Send packet size " << pkt->size << "bytes");
        clientSock->Send(&pkt->size, sizeof(pkt->size));
        now = std::chrono::high_resolution_clock::now();
        auto deltaSend = now - lastSendT;

        std::stringstream sst;
        sst << "stats\n";
        sst << "encode " << int(deltaEncode.count() / 1e6) << "ms\n";
        sst << "send " << int(deltaSend.count() / 1e3) << "us\n";
        sst << "bytes sent " << pkt->size << "bytes";
        cornerAnnotation->SetText(sst.str().c_str());

        vtkLog(TRACE, << "Send " << deltaSend.count() / 1e9 << "ms");
        vtkLog(TRACE, << "================================");
        clientSock->Send(pkt->data, pkt->size);
      };

      // Begin the encoding on server.
      vtkLog(TRACE, << "Encoding " << frame->pts);
      ::lastEncodeT = std::chrono::high_resolution_clock::now();
      bool success = ffmpeg::encoding::encode_rgba32_frame(
        ::RGBA32Data->GetPointer(0), frame, ctx, sws_context, pkt, packetHandler);
      if (!success)
      {
        vtkLog(WARNING, << "Failed to encode frame");
        continue;
      }
      /* Finished encoding. Inform client. */
      int finishMsgSize[1] = { 6 };
      clientSock->Send(finishMsgSize, sizeof(int));
      clientSock->Send(finishMsg, strlen(finishMsg) * sizeof(char));
      ++frame->pts;
    }
  }

  /* Cleanup decoder */
  avcodec_free_context(&ctx);
  ::TearDownEncoderFrame();
  av_packet_free(&pkt);

  if (clientSock != nullptr && clientSock->GetConnected())
  {
    clientSock->CloseSocket();
  }
  socket->CloseSocket();

  return EXIT_SUCCESS;
}
