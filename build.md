# Build steps

### Get source
```bash
$ git clone --recursive https://gitlab.kitware.com/jaswant.panchumarti/streamvtkffmpegtest.git
$ cd streamvtkffmpegtest
```
## Linux
### Install ffmpeg libraries
On ubuntu, chances are you already have these libraries. Just in case you 
don't, here's all of them.

```bash
sudo apt install libavformat-dev libavcodec-dev libavutil-dev libswscale-dev libswresample-dev libvpx-dev
```
You may also download ffmpeg release binaries from [ffmpeg.org](https://www.ffmpeg.org/download.html#build-linux)


### Build/Install VTK
See [VTK - getting started on linux](https://gitlab.kitware.com/vtk/vtk/-/blob/master/Documentation/dev/getting_started_linux.md)

### Compile
The project enfoces out-of-source builds. So let's create a separate build directory. If you've downloaded FFMPEG binaries, now would be a good time to set the `FFMPEG_ROOT` environment variable to point to the FFMPEG root directory (the one with bin/, lib/)

```bash
$ mkdir build
$ cd build
$ cmake -GNinja -DVTKENCDEC_WRAP_PYTHON=ON #-DBUILD_EXAMPLE=ON, default
$ ninja
```

## Windows
### Install ffmpeg libraries
I'd suggest downloading prebuilt release binaries from [here](https://github.com/BtbN/FFmpeg-Builds/releases). Look for zips with the suffix `latest-win64-lgpl-shared`. Download any version as long as it's greater than 4.3. After extracting the zip, please set the environment variable `FFMPEG_ROOT=<extracted dir path>` and add `FFMPEG_ROOT/bin` to PATH.

### Build/Install VTK
See [VTK - Build on Windows](https://gitlab.kitware.com/vtk/vtk/-/blob/master/Documentation/dev/build_windows_vs.md)

After building VTK, point `VTK_DIR` environment variable to your VTK build/install tree. Also, add `VTK_DIR/bin` to path.

### Compile
Check that your `FFMPEG_ROOT` is set. Check again. Open up powershell.
```bash
> mkdir build
> cd build
> cmake -S ../ -B .
> cmake --build .
```
