OpenCV/ffmpeg CLI do not offer low-level capabilities that are necessary to achieive this.  
Applications with similar goals - 
1. Nomachine uses vp8 or mjpeg to encode-decode a remote desktop session. They use the ffmpeg libraries for this purpose. See https://www.nomachine.com/opensource.
2. Parsec sdk is open source, but the libraries are closed source and it also uses microsoft's h264 on windows and ffmpeg on ubuntu. See https://parsec.app/blog/play-any-pc-game-on-your-linux-machine-with-cloud-gaming-or-game-streaming-a64e9925462f
3. Moonlight uses an entirely different networking stack and it looks like the encode/decode is tied into this? It uses ENet instead of tcp/udp.

## Implementation
The client and server are two separate applications that communicate using simple `vtkSocket` objects over TCP. The server sets up a simple vtk pipeline - code copied from [cylinder](https://kitware.github.io/vtk-examples/site/Cxx/GeometricObjects/CylinderExample/). The client on the other hand sets up only the renderer, render window and an interactor. When you interact in the client - move your mouse - the camera and window size of the client render window are captured and sent to the server over a socket. 

The server loads up the camera and window size (that it got from the client) and renders the cylinder into the server's render window. After this is done, it captures a single frame and sends it over for encoding. Encoding happens on the server. If the encoder has an output data packet, the server communicates this data packet back to the client. Sometimes, the encoder might not give an output based on the gop_size or i-frame settings. The client detects the input data packet(that it received from the server) and decodes it into an RGBA 32bpp frame that will be drawn in the client render window.

The key here is whenever a new interaction begins, the server enforces an I-Frame making sure that future frames are better compressed - because the contents of a macroblock closely resemble previous frames' macroblocks.

An encoder typically compresses an input frame based on some information in the previous frames. This compressed data packet is sent over to the client which decompresses it (decoding) back to an rgba 32bpp buffer that can be drawn by vtk render window with the DrawPixels method.
