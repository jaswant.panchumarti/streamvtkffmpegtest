You may obtain similar results by running the example `server` and `client` applications which print a summary that contains average timing and compression ratio.

There was no set procedure used to benchmark the various codecs. I randomly zoomed, panned and rotated the cylinder for about 200-300 frames to generate enough samples.

||Codec|Compression Ratio|Latency|GPU->CPU copy|Encode + Scale|Send|Receive|Decode + Scale|CPU->GPU copy|Notes|
|-|---|--|--|--|--|--|--|--|--|--|
|1.|HEVC/H.265|1150|22ms|6ms|20ms + 11ms|12us|16us|896us + 824us|1ms||
|2.|vp9|1253|18ms|2ms|6ms + 4ms|2us|2us|834us + 789us|1ms||
|3.|libopenjpeg|59|116ms|4ms|59ms + 668us|27us|9us|684us + 600us|9us||
|4.|bitmap|1|14ms|6ms|755us + 1ms|978us|933us|2ms + 2ms|1ms||
|5.|libaom-av1|2373|143ms|6ms|123ms + 4ms|4us|4us|805us + 744us|1ms|svt-av1 is known to be better|
|6.|vtkJPEGReader/Writer|39|101ms|5ms|66ms + 0|37us|13us|25ms + 0|1ms||
|7.|vtkPNGReader/Writer|114|180ms|8ms|160ms + 0|13us|4us|9ms + 0|1ms||

## Compression Ratio
Describes how much the encoder compressed the input image. This is equal to `size of input image to the encoder` / `size of compressed output packet from encoder`. In the above table, all codecs except bitmap have values greater than 1. Bitmap does not do any compression, it is lossless and sends the image as it is to the client.

## GPU->CPU copy
Before encoding begins, the server must grab the final render from the GPU's 'draw' frame buffer. This is accomplished with `vtkOpenGLRenderWindow::ReadPixels()` which populates an unsigned char array with r,g,b,a tuples. The result array is then input to the encoder. This copy process can affect latency. It strongly depends on the GPU, driver and OS. Every GPU vendor has their own implementation of `glReadPixels()`.

## Encode + Scale
The encode time corresponds to an interval starting from `avcodec_send_frame()` to an `avcodec_receive_packet()`. Prior to encoding, the input image is converted into an alternate pixel format suitable for the encoder. The conversion is typically from RGBA->YUV420P. Encoders can efficiently compress planar pixel formats.  VTK render window's output is RGBA and not a planar pixel format. The scale time for vtk[JPEG, PNG][Reader/Writer] is 0 because these accept `rgba32` pixel format.

## Send
Time taken for the server to send the compressed packet over a socket to connected client. 

## Receive
Time taken for the client to receive the compressed packet over a socket from the server.

### Note
When both client and server are on same machine or network, send/recv takes insignificant time.

## Decode + Scale
The decode time corresponds to an interval starting from `avcodec_send_packet()` to an `avcodec_receive_frame()`. After decoding, the output image may be in a planar pixel format (YUV420P). VTK renderwindow cannot draw images with a planar pixel format. This explains the software scaling done to convert YUV420P -> RGBA. The scale time for vtk[JPEG, PNG][Reader/Writer] is 0 because these output `rgba32` pixel format.

## CPU->GPU copy
After the conversion to RGBA, the image buffer on the CPU is sent to the GPU with `vtkOpenGLRenderWindow::DrawPixels()`. This function internally calls `glDrawPixels()`.



